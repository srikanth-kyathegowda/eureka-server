package com.example.eureka.eurekaserver;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;
import org.springframework.cloud.netflix.zuul.EnableZuulProxy;

@EnableZuulProxy
@EnableEurekaServer
@SpringBootApplication
public class EurekaServerApplication {

	public static void main(String[] args) {
		SpringApplication.run(EurekaServerApplication.class, args);
	}
}


//to generate image file - docker build -f Dockerfile -t service1 .
//to see list of images created - docker images
//to start application using image name - docker run -p 3001:3001 service1
//to get docker machine ip - docker-machine ip
//to kill particular container/application - docker kill CONTAINER_ID
//to list all the containers - docker PS
//to fetch log for particular process/container - docker logs --details CONTAINER_ID

